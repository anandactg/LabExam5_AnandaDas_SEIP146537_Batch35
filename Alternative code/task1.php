<?php
class array_sort{
    protected $_asort;
    public function __construct(array $asort)
    {
        $this->_asort = $asort;
    }
    public function mySort(){
        sort($this->_asort);
        return $this->_asort;
    }
}

$sortarray = new array_sort(array(5,7,1,6,2));
print_r($sortarray->mySort())."<br>";