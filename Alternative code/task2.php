<?php
    $startTime = new DateTime("1992-12-24");
    $endTime = new DateTime("2016-10-29");
    $result = $endTime->diff($startTime);
    $show = $result->format("%y years,%m months,%days");
    echo $show;
