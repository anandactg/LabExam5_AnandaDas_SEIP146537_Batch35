<?php
class calculator{
    public $num1;
    public $num2;
    public function MyCalculator($num1,$num2){
        $this->num1 = $num1;
        $this->num2 = $num2;
    }
    public function add(){
        echo ($this->num1+$this->num2)."<br>";
    }
    public function substract(){
        if($this->num1>$this->num2){
            echo ($this->num1-$this->num2)."<br>";
        }else{
            echo ($this->num2-$this->num1)."<br>";
        }
    }
    public function multiply(){
        echo ($this->num1*$this->num2)."<br>";
    }
    public function divide(){
        if($this->num1>$this->num2){
            echo ($this->num1/$this->num2)."<br>";
        }else{
            echo ($this->num2/$this->num1)."<br>";
        }
    }
}

$myObj = new calculator();
$myObj->MyCalculator(5,8);
$myObj->add();
$myObj->substract();
$myObj->multiply();
$myObj->divide();