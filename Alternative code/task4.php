<?php
class factorial_of_a_number{
    protected $number;
    public function __construct($number)
    {
        if(!is_int($number)){
            throw new InvalidArgumentException('not a number or missing argument');
        }
        $this->number = $number;
    }
    public function result(){
        $a=1;
        for($b=1;$b<=$this->number;$b++){
            $a = $a*$b;
        }
        return $a;
    }
}

$newfactorial = New factorial_of_a_number(3);
echo $newfactorial->result();