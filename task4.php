<?php

 class factorial_of_a_number

 {

    protected $_number;

public function __construct($number)

{

         if (!is_int($number))

            {

            throw new InvalidArgumentException('Not a number or missing argument');

 }

 $this->_number = $number;

 }
    public function result(){

            $factorial=1;
        for($num=$this->_number;$num>1;$num--){

            $factorial=$factorial*$num;
        }
        return $this->_number=$factorial;
    }
}

$newfactorial = New factorial_of_a_number(7);

echo $newfactorial->result();

?>